﻿using UnityEngine;
using System.Collections;

public class PieceHolder : MonoBehaviour {

    bool isOcupied = false;
    int collisionCounter = 0;

    [SerializeField]
    bool debug = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        if (debug)
            print(collisionCounter);

	}

    public bool Occupied()
    {
        return (collisionCounter > 0);
    }

    public void AddOcupied()
    {
        collisionCounter++;
    }

    public void RemoveOcupied()
    {
        if (collisionCounter >= 1)
            collisionCounter--;
    }    
}
