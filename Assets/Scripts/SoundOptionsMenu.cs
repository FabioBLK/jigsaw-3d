﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SoundOptionsMenu : MonoBehaviour {

    [SerializeField]
    GameObject titleCanvas;

    [SerializeField]
    GameObject soundOptions;

    [SerializeField]
    Button musicButton;

    [SerializeField]
    Button sfxButton;

    [SerializeField]
    Sprite soundImageOn;

    [SerializeField]
    Sprite soundImageOff;

    bool musicOn = true;
    bool sfxOn = true;

    GameObject[] canvasChild;

    // Use this for initialization
    void Start () {
        if (SceneController.CurrentLoadedLevel() == 0)
        {
            canvasChild = new GameObject[titleCanvas.transform.childCount];

            for (int i = 0; i < canvasChild.Length; i++)
            {
                canvasChild[i] = titleCanvas.transform.GetChild(i).gameObject;
            }
        }

    }
	
    public void SoundEnterButtonPressed()
    {
        SFXManager.instance.PlayClickSound();

        foreach (GameObject go in canvasChild)
        {
            go.SetActive(false);
        }
        soundOptions.SetActive(true);

        CheckIfSoundAndSFXActive();

    }

    public void CheckIfSoundAndSFXActive()
    {
        if (MusicManager.instance.IsMusicMute())
        {
            musicButton.image.sprite = soundImageOff;
            musicOn = false;
        }
        if (SFXManager.instance.IsSFXMute())
        {
            sfxButton.image.sprite = soundImageOff;
            sfxOn = false;
        }
    }

    public void SoundExitButtonPressed()
    {
        SFXManager.instance.PlayClickSound();

        foreach (GameObject go in canvasChild)
        {
            go.SetActive(true);
        }
        soundOptions.SetActive(false);
    }

    public void MusicOptionsPressed()
    {
        SFXManager.instance.PlayClickSound();

        musicOn = !musicOn;

        if (musicOn)
        {
            musicButton.image.sprite = soundImageOn;
            MusicManager.instance.MuteMusic(false);
        }
        else
        {
            musicButton.image.sprite = soundImageOff;
            MusicManager.instance.MuteMusic(true);
        }
    }

    public void SFXOptionsPressed()
    {
        SFXManager.instance.PlayClickSound();

        sfxOn = !sfxOn;

        if (sfxOn)
        {
            sfxButton.image.sprite = soundImageOn;
            SFXManager.instance.MuteSFX(false);
        }
        else
        {
            sfxButton.image.sprite = soundImageOff;
            SFXManager.instance.MuteSFX(true);
        }
    }
}
