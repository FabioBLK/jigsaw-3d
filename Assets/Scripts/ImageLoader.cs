﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class ImageLoader : MonoBehaviour {

    [SerializeField]
    GameObject customBox;
    //public GameObject cube2;

    int maxResolutionX = 1024;
    int maxResolutionY = 1024;

    Renderer myRender;

    string imgPath;

    AndroidJavaClass ajc;
    AndroidJavaObject ajo, obj;

    [DllImport("__Internal")]
    private static extern void OpenVideoPicker(string game_object_name, string function_name);

    public void Start()
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        ajc = new AndroidJavaClass("com.FabioBLK.imageloader.ImageLoadPlugin");
        ajc.CallStatic("start");
        ajo = ajc.GetStatic<AndroidJavaObject>("instance");
        imgPath = ajc.CallStatic<string>("returnImagePath");
#endif

		#if UNITY_EDITOR
		//imgPath = @"/Users/fabio/Documents/SantasLittleHelper.png";
		//StartCoroutine(LoadPic());
		#endif

    }

    public void CallImagePath()
    {
#if UNITY_ANDROID
        ajc.CallStatic("openGallery");
#endif

#if UNITY_IOS
        OpenVideoPicker( "GameObject", "VideoPicked" );
        Debug.Log ("In Unity, call invokeMediaBrowser");
#endif


    }

    public void LoadPathString()
    {
#if UNITY_ANDROID
        imgPath = ajc.CallStatic<string>("returnImagePath");
        //Debug.Log(imgPath);
        StartCoroutine(LoadPic());
#endif
    }

	public void ChangeImgPath(string path){
	
		imgPath = path;
        StartCoroutine(LoadPic());
	
	}


    void Update()
    {
        //if (Input.GetMouseButtonDown(0))
        //{
        //    CallImagePath();
        //}

#if UNITY_ANDROID && !UNITY_EDITOR
        if (imgPath != ajc.CallStatic<string>("returnImagePath"))
        {
            LoadPathString();
        }
#endif

#if UNITY_IOS
        //print("Teste");
#endif

    }

    IEnumerator LoadPic()
    {
        // Start a download of the given URL
        WWW www = new WWW("file://" + imgPath);
		//print ("The path is " + imgPath);

        // Wait for download to complete
        yield return www;

        // assign texture
		
		Renderer renderer = customBox.GetComponent<ItemSelect>().PickMyRenderer();


		//Renderer renderer = customBox.GetComponent<Renderer> ();
		
        //renderer.material.mainTexture = www.texture;

		#if UNITY_ANDROID
        renderer.material.mainTexture = new Texture2D(512, 512, TextureFormat.DXT5, true, false);
        #endif

        #if UNITY_IOS
        renderer.material.mainTexture = new Texture2D(512, 512, TextureFormat.PVRTC_RGB4, true, false);
        #endif

        www.LoadImageIntoTexture(renderer.material.mainTexture as Texture2D);
        customBox.GetComponent<ItemSelect>().SetMyRenderer(renderer);


		//renderer.material.mainTexture = new Texture2D(512, 512, TextureFormat.PVRTC_RGB4, true, false);
		//www.LoadImageIntoTexture(renderer.material.mainTexture as Texture2D);
		//customBox.GetComponent<Renderer>().material = renderer.material;

		www.Dispose();
        www = null;
        

        /*
        Texture2D texture2D = www.texture;

        if (texture2D.width > maxResolutionX || texture2D.height > maxResolutionY)
        {
            float widthRatio = (float)maxResolutionX / texture2D.width;
            float heightRatio = (float)maxResolutionY / texture2D.height;
            if (widthRatio < heightRatio)
            {
                texture2D = Resize(texture2D, (int)(texture2D.width * widthRatio), (int)(texture2D.height * widthRatio));
            }
            else {
                texture2D = Resize(texture2D, (int)(texture2D.width * heightRatio), (int)(texture2D.height * heightRatio));
            }

            // apply the texture to the renderer
            Renderer renderer2 = cube2.GetComponent<Renderer>();
            renderer2.material.mainTexture = texture2D;
        }
        */
    }

    // returns a texture resized to the given dimensions
    public Texture2D Resize(Texture2D source, int width, int height)
    {
        // initialize render texture with target width and height
        RenderTexture rt = new RenderTexture(width, height, 32);
        // set as the active render texture
        RenderTexture.active = rt;
        // render source texture to the render texture
        GL.PushMatrix();
        GL.LoadPixelMatrix(0, width, height, 0);
        Graphics.DrawTexture(new Rect(0, 0, width, height), source);
        GL.PopMatrix();
        // initialize destination texture with target width and height
        Texture2D dest = new Texture2D(width, height);
        // copy render texture to the destination texture
        dest.ReadPixels(new Rect(0, 0, width, height), 0, 0);
        dest.Apply();
        // resume rendering to the main window
        RenderTexture.active = null;
        return dest;
    }

    public void VideoPicked( string path ){
        Debug.Log ("---->VideoPicked");
        Debug.Log("I Received this: " + path );
        ChangeImgPath (path);

    }
}
