﻿using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class VideoPicker : MonoBehaviour {

	public Texture2D shareButtonImage; // Use this for initialization

	[DllImport("__Internal")]
	private static extern void OpenVideoPicker(string game_object_name, string function_name);

	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (Input.GetMouseButtonDown (0)) {
		
			OpenVideoPicker( "GameObject", "VideoPicked" );
			Debug.Log ("In Unity, call invokeMediaBrowser");

		}
	}

	public void VideoPicked( string path ){
		Debug.Log ("---->VideoPicked");
		Debug.Log("I Received this: " + path );
		GetComponent<ImageLoader> ().ChangeImgPath (path);

	}
}