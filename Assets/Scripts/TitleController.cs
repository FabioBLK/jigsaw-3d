﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TitleController : MonoBehaviour {

    #if UNITY_ANDROID
    SocialController socialController;
    #endif
    #if UNITY_IOS
    SocialControllerIOS socialControllerIOS;
    #endif

    [SerializeField]
    GameObject titleCanvas;

   [SerializeField]
    Button achievementButton;

    [SerializeField]
    Button leaderboardsButton;

    [SerializeField]
    Text titleText;

    [SerializeField]
    Text leaderboardsText;

    [SerializeField]
    Text achievementsText;

    [SerializeField]
    Text startText;

    [SerializeField]
    Text musicText;

    [SerializeField]
    Text sfxText;

    [SerializeField]
    Text credits1;

    [SerializeField]
    Text musicBy;


    // Use this for initialization
    void Start () {

        StartTitleScreenText();

        Time.timeScale = 1;

#if UNITY_ANDROID
        socialController = FindObjectOfType<SocialController>();
#endif

#if UNITY_IOS
        socialControllerIOS = FindObjectOfType<SocialControllerIOS>();
#endif

        Invoke("ShowTitleCanvas", 2.5f);
    }

    void Update()
    {
#if UNITY_ANDROID
        
        leaderboardsButton.interactable = !socialController.ShowWarning();
        achievementButton.interactable = !socialController.ShowWarning();

#endif

#if UNITY_IOS

        leaderboardsButton.interactable = !socialControllerIOS.ShowWarning();
        achievementButton.interactable = !socialControllerIOS.ShowWarning();

#endif
    }

    void ShowTitleCanvas()
    {
        titleCanvas.SetActive(true);
    }

    void StartTitleScreenText()
    {
        titleText.text = LanguageStrings.instance.TextString("game_title");
        leaderboardsText.text = LanguageStrings.instance.TextString("leaderboards");
        achievementsText.text = LanguageStrings.instance.TextString("achievements");
        startText.text = LanguageStrings.instance.TextString("play_button");
        musicText.text = LanguageStrings.instance.TextString("music");
        sfxText.text = LanguageStrings.instance.TextString("sfx");
        credits1.text = LanguageStrings.instance.TextString("credits1");
        musicBy.text = LanguageStrings.instance.TextString("music_by");

    }

    public void StartedPressed()
    {
        SFXManager.instance.PlayClickSound();
        MusicManager.instance.ChangeMusic(2);
        FindObjectOfType<SceneFadeInOut>().StartSceneEnd();
    }

    public void LeaderBoardsPressed()
    {
        SFXManager.instance.PlayClickSound();
#if UNITY_ANDROID
        socialController.ShowMainLeaderBoard();
#endif

#if UNITY_IOS
        socialControllerIOS.OnShowLeaderBoard();
#endif

    }

    public void AchievementsPressed()
    {
        SFXManager.instance.PlayClickSound();
#if UNITY_ANDROID
        socialController.ShowAchievements();
#endif

#if UNITY_IOS
        socialControllerIOS.ShowAchievements();
#endif
    }

}
