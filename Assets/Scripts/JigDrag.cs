﻿using UnityEngine;
using System.Collections;

public class JigDrag : MonoBehaviour {

    Camera myCamera;
    CameraDrag cameraDrag;
    GameControl gameControl;

    Vector3 colliderPos = Vector3.zero;
    Vector3 startPos = Vector3.zero;

    Vector3 originalBoxSize;

    Quaternion startRot = Quaternion.identity;

    bool draging = false;
    bool canBePlaced = false;
    bool notStarted = true;
    bool paused = false;
    bool forceOutline = false;

    Rigidbody rb;
    BoxCollider myBox;
    Renderer myRenderer;

    [SerializeField]
    Material originalMaterial;

    [SerializeField]
    Material outlineMaterial;

    [SerializeField]
    float maxXPosition = 2.3f;

    [SerializeField]
    float minXPosition = -2.3f;

    [SerializeField]
    float maxYPosition = 1.3f;

    [SerializeField]
    float minYPosition = -1.3f;

    [SerializeField]
    bool debug = false;

    // Use this for initialization
    void Start () {
        gameControl = FindObjectOfType<GameControl>();
        myCamera = Camera.main;
        cameraDrag = myCamera.GetComponent<CameraDrag>();
        startPos = new Vector3(transform.position.x, transform.position.y, transform.position.z);
        startRot = transform.rotation;
        rb = GetComponent<Rigidbody>();
        myBox = GetComponent<BoxCollider>();
        originalBoxSize = myBox.size;
        myRenderer = GetComponent<Renderer>();
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (debug)
            print(GetComponent<Renderer>().material);

        if (Input.GetMouseButtonDown(0) && notStarted)
        {
            RandomPosition();
        }

        if (draging || !GetComponent<Collider>().enabled)
        {
            //print("Original");
            GetComponent<Renderer>().material = originalMaterial;
            forceOutline = false;
        }

        // ********* add this to enable snap on the possible position that the piece may fit *********
        //SnapThis();
    }

    private void SnapThis()
    {
        if (!draging)
            return;

        float distance = Vector3.Distance(transform.position, colliderPos);

        if (distance <= 0.2f)
        {
            if (draging)
                transform.position = new Vector3(colliderPos.x, 0.15f, colliderPos.z); //this is the snap position    
        }
    }

    void OnMouseDrag()
    {
        if (Input.touchCount > 1 || paused)
            return;

        draging = true;
        rb.isKinematic = true;
        rb.useGravity = false;
        myBox.size = new Vector3(0.03f, 0.05f, myBox.size.z);

        float distanceToScreen = myCamera.WorldToScreenPoint(transform.position).z;
        Vector3 newPos = myCamera.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, distanceToScreen));
        transform.position = new Vector3(Mathf.Clamp(newPos.x, minXPosition, maxXPosition), 0.15f, Mathf.Clamp(newPos.z + 0.15f, minYPosition, maxYPosition));
        canBePlaced = false;
        cameraDrag.AllowDrag(false);
    }

    void OnMouseUp()
    {
        rb.isKinematic = false;
        rb.useGravity = true;
        
        draging = false;
        cameraDrag.AllowDrag(true);

        if (Vector3.Distance(transform.position, startPos) < 0.2f)
        {
            //DisableAtCorrectPosition();
            canBePlaced = true;
        }
        GetComponent<Renderer>().material = outlineMaterial;
    }

    private void DisableAtCorrectPosition()
    {
        SFXManager.instance.PlayMatchPlaceSound();
        transform.rotation = startRot;
        transform.position = startPos;
        rb.isKinematic = true;
        rb.useGravity = false;
        GetComponent<Collider>().enabled = false;
        GetComponent<Renderer>().material = originalMaterial;
        gameControl.RemovePiece();
    }

    void RandomPosition()
    {
        
        Vector3 newPos = new Vector3(Random.Range(minXPosition, maxXPosition), 0.5f, Random.Range(minYPosition, maxYPosition));
        transform.position = newPos;
        myBox.enabled = true;
        rb.isKinematic = false;
        rb.useGravity = true;
        notStarted = false;
        forceOutline = true;
    }

    public void PauseJig(bool value)
    {
        paused = value;
    }

    void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.CompareTag("JigHolder"))
        {
            GetComponent<Renderer>().material = outlineMaterial;
            colliderPos = col.transform.position;
            forceOutline = true;
            GetComponent<Renderer>().material = outlineMaterial;
            /*
            PieceHolder pieceHolder = col.GetComponent<PieceHolder>();
            
            if (startPos == col.transform.position && !pieceHolder.Occupied())
            {
                canBePlaced = true;
            }
            else
            {
                pieceHolder.AddOcupied();
                canBePlaced = false;
            }
            */
        }
    }

    void OnCollisionEnter(Collision col)
    {
        GetComponent<Renderer>().material = outlineMaterial;
        if (col.gameObject.CompareTag("Jig"))
        {
            canBePlaced = false;
            myBox.size = originalBoxSize;
            //print("Ocupado");
        }
        else
        {
            if (canBePlaced)
                DisableAtCorrectPosition();

            myBox.size = originalBoxSize;
        }
    }

    /*
    void OnTriggerExit(Collider col)
    {
        if (col.gameObject.tag == "JigHolder")
        {
            PieceHolder pieceHolder = col.GetComponent<PieceHolder>();
            pieceHolder.RemoveOcupied();
        }
    }
    */
}
