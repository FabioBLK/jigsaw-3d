﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameControl : MonoBehaviour {

    enum puzzleSize {size4, size16, size36, size64 }
    [SerializeField]
    puzzleSize currentSize;

    bool pauseGame = false;

    JigDrag[] jigs;

    TextMesh numberText;

    TextMesh infoText;

    DigitalClock digitalClock;
    #if UNITY_ANDROID
    SocialController socialController;
    #endif
    #if UNITY_IOS
	SocialControllerIOS socialControllerIOS;
    #endif
    UnityAdsControl unityAdsControl;

    int counter = 0;

    [SerializeField]
    Texture currentImage;

    [SerializeField]
    Material jigMap;
    [SerializeField]
    Material jigMapOutline;

    [SerializeField]
    GameObject jigPrefab16;
    [SerializeField]
    GameObject holderPrefab16;
    [SerializeField]
    GameObject jigPrefab32;
    [SerializeField]
    GameObject holderPrefab32;
    [SerializeField]
    GameObject jigPrefab64;
    [SerializeField]
    GameObject holderPrefab64;



	// Use this for initialization
	
    void Awake()
    {
        //if (Screen.width * Screen.height > 2073600)
          //  Screen.SetResolution(Screen.width / 2, Screen.height / 2, true);

        SelectionManager selectionManager = FindObjectOfType<SelectionManager>();
        if (selectionManager)
        {
            currentImage = selectionManager.SelectedTexture();
            currentSize = (puzzleSize)selectionManager.SelectedPuzzleSize();
        }

    }

    void Start () {

        StartGameControl();
        digitalClock = FindObjectOfType<DigitalClock>();
        unityAdsControl = FindObjectOfType<UnityAdsControl>();

#if UNITY_IOS
			socialControllerIOS = FindObjectOfType<SocialControllerIOS>();
#endif

#if UNITY_ANDROID
            socialController = FindObjectOfType<SocialController>();
        #endif


    }

    public void StartGameControl()
    {
        DefinePuzzle();
        jigs = FindObjectsOfType<JigDrag>();
        counter = jigs.Length;
        numberText.text = counter.ToString();
    }

    private void DefinePuzzle()
    {
        jigMap.mainTexture = currentImage;
        jigMapOutline.mainTexture = currentImage;
        
        GameObject currentHolderPrefab = Instantiate(HolderPrefab(currentSize)) as GameObject;
        GameObject currentJigPrefab = Instantiate(PuzzlePrefab(currentSize)) as GameObject;
        numberText = currentHolderPrefab.transform.GetChild(1).GetComponent<TextMesh>();
        infoText = currentHolderPrefab.transform.GetChild(2).GetComponent<TextMesh>();
        infoText.text = LanguageStrings.instance.TextString("pieces_left");

    }

    GameObject HolderPrefab(puzzleSize size)
    {
        if (size == puzzleSize.size36)
            return holderPrefab32;
        else if (size == puzzleSize.size16)
            return holderPrefab16;
        else if (size == puzzleSize.size64)
            return holderPrefab64;
        else
        {
            print("não achei");
            return holderPrefab32;
        }
    }

    GameObject PuzzlePrefab(puzzleSize size)
    {
        if (size == puzzleSize.size36)
            return jigPrefab32;
        else if (size == puzzleSize.size16)
            return jigPrefab16;
        else if (size == puzzleSize.size64)
            return jigPrefab64;
        else
        {
            print("não achei");
            return jigPrefab32;
        }
    }

    public void PauseGame()
    {
        if (!pauseGame)
            Time.timeScale = 0;
        else
            Time.timeScale = 1;

        pauseGame = !pauseGame;
        foreach(JigDrag jig in jigs)
        {
            jig.PauseJig(pauseGame);
        }
    }

    public void RemovePiece()
    {
        counter--;
        numberText.text = counter.ToString();

        if (counter <= 0)
            GameFinished();
    }

    void GameFinished()
    {
        numberText.text = " ";
        infoText.characterSize = 0.44f;
        infoText.text = LanguageStrings.instance.TextString("congratulations");
        unityAdsControl.HideLoadedBanner();
        CallGameOverScreen();
    }

    void CallGameOverScreen()
    {
        digitalClock.StopTimer();
        GetComponent<MyUiControl>().SetFinalTime(digitalClock.ReturnTime());
        SendAndShowLeaderBoardsInfo(digitalClock.ReturnFloatTime());
        CheckAndSendAchievements(digitalClock.ReturnFloatTime());
		GetComponent<MyUiControl>().GameOverScreen();
    }

    void SendAndShowLeaderBoardsInfo(float time)
    {
		#if UNITY_IOS
			if (currentSize == puzzleSize.size16)
			{
				socialControllerIOS.SetCurrentLeaderBoard("16_Pieces_Record");
			}
			else if (currentSize == puzzleSize.size36)
			{
				socialControllerIOS.SetCurrentLeaderBoard("36_Pieces_Record");
			}
			else if (currentSize == puzzleSize.size64)
			{
				socialControllerIOS.SetCurrentLeaderBoard("64_Pieces_Record");
			}
			socialControllerIOS.OnAddScoreToLeaderBorad((long)time);
			//socialControllerIOS.OnShowLeaderBoard();
		#endif

        #if UNITY_ANDROID
            if (currentSize == puzzleSize.size16)
            {
                socialController.SetCurrentLeaderBoard(TimeRecords.leaderboard_16_pieces_record);
            }
            else if (currentSize == puzzleSize.size36)
            {
                socialController.SetCurrentLeaderBoard(TimeRecords.leaderboard_36_pieces_record);
            }
            else if (currentSize == puzzleSize.size64)
            {
                socialController.SetCurrentLeaderBoard(TimeRecords.leaderboard_64_pieces_record);
            }

            socialController.OnAddScoreToLeaderBorad((long)time*1000);
            //socialController.OnShowLeaderBoard();
        #endif
    }

    public void ShowLeaderBoardButton()
    {
        SFXManager.instance.PlayClickSound();
#if UNITY_ANDROID
        if (!socialController.ShowWarning())
            socialController.OnShowLeaderBoard();
#endif

#if UNITY_IOS
        if (!socialControllerIOS.ShowWarning())
            socialControllerIOS.OnShowLeaderBoard();
#endif
    }

    void CheckAndSendAchievements(float finishTime)
    {
#if UNITY_ANDROID
        if (currentSize == puzzleSize.size16)
        {
            socialController.UnlockAchievement(TimeRecords.achievement_just_to_get_started);
            if (finishTime < 61)
            {
                socialController.UnlockAchievement(TimeRecords.achievement_nice_catch);
            }
        }

        else if (currentSize == puzzleSize.size36)
        {
            socialController.UnlockAchievement(TimeRecords.achievement_the_way_i_like_it);
            if (finishTime < 241)
            {
                socialController.UnlockAchievement(TimeRecords.achievement_my_average);
            }
        }

        else if (currentSize == puzzleSize.size64)
        {
            socialController.UnlockAchievement(TimeRecords.achievement_im_up_to_the_task);
            if (finishTime < 481)
            {
                socialController.UnlockAchievement(TimeRecords.achievement_im_pretty_good);
            }
        }
#endif

#if UNITY_IOS
        if (currentSize == puzzleSize.size16)
        {
			socialControllerIOS.UnlockAchievement("just_to_get_started");
            if (finishTime < 61)
            {
				socialControllerIOS.UnlockAchievement("nice_catch");
            }
        }

        else if (currentSize == puzzleSize.size36)
        {
			socialControllerIOS.UnlockAchievement("the_way_i_like_it");
            if (finishTime < 241)
            {
				socialControllerIOS.UnlockAchievement("my_average");
            }
        }

        else if (currentSize == puzzleSize.size64)
        {
			socialControllerIOS.UnlockAchievement("im_up_to_the_task");
            if (finishTime < 481)
            {
				socialControllerIOS.UnlockAchievement("im_pretty_good");
            }
        }
#endif


    }
}
