﻿using UnityEngine;
using System.Collections;

public class ItemSelect : MonoBehaviour {

    bool onShelf = true;
    Vector3 startPosition = Vector3.zero;
    Vector3 startEuler = Vector3.zero;
    Texture myTexture;
	float rotateDegrees = 0;

    ImageLoader imgLoader;

    [SerializeField]
    Transform previewPlace;

    [SerializeField]
    float speed = 1;

	[SerializeField]
	bool leftSide = false;

    [SerializeField]
    bool customPicture;

    [SerializeField]
    Material customMaterial;

    Renderer myRenderer;

    SelectionManager selectionManager;

	// Use this for initialization
	void Start () {
        selectionManager = FindObjectOfType<SelectionManager>();
        startPosition = transform.position;
        startEuler = transform.eulerAngles;
        myTexture = GetComponent<Renderer>().material.mainTexture;
        myRenderer = GetComponent<Renderer>();
        imgLoader = FindObjectOfType<ImageLoader>();

        if (customMaterial == null)
            customMaterial = GetComponent<Renderer>().material;

		if (leftSide)
			rotateDegrees = 359.8f;
	}
	
	// Update is called once per frame
	void Update () {
	
        if (!onShelf)
        {
            Vector3 pos = Vector3.Lerp(transform.position, previewPlace.position, Time.deltaTime * speed);
            transform.position = pos;

            Vector3 rot = Vector3.Lerp(transform.eulerAngles, new Vector3(0, rotateDegrees, 180), Time.deltaTime * speed);
            transform.eulerAngles = rot;
        }
        else
        {
            Vector3 pos = Vector3.Lerp(transform.position, startPosition, Time.deltaTime * speed);
            transform.position = pos;

            Vector3 rot = Vector3.Lerp(transform.eulerAngles, startEuler, Time.deltaTime * speed);
            transform.eulerAngles = rot;
        }
       
	}

    void OnMouseDown()
    {
        if (customPicture)
        {
#if UNITY_ANDROID
            GameObject go = new GameObject();
            go.AddComponent<CheckPermissionsAnd>();
            CheckPermissionsAnd cpa = go.gameObject.GetComponent<CheckPermissionsAnd>();
            bool isGranted = cpa.IsPermissionGranted();
            if (!isGranted)
            {
                //cpa.ShowGetMessage();
                return;
            }
#endif
            imgLoader.CallImagePath();
            return;
        }

        SFXManager.instance.PlayWooshSound();
        selectionManager.BoxSelected(this, myTexture);
        onShelf = false;
    }

    public void GoToShelf()
    {
        onShelf = true;
    }

    public Renderer PickMyRenderer()
    {
        return myRenderer;
    }

    public void SetMyRenderer(Renderer newRenderer)
    {
        customMaterial.mainTexture = newRenderer.material.mainTexture;
        myRenderer.material = customMaterial;
        selectionManager.BoxSelected(this, GetComponent<Renderer>().material.mainTexture);

        SFXManager.instance.PlayWooshSound();
        onShelf = false;
    }

}
