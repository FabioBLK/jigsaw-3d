﻿using UnityEngine;
using System.Collections;

public class PicturePreview : MonoBehaviour {

    [SerializeField]
    Transform previewSpot;

    [SerializeField]
    bool previewMode = false;

    [SerializeField]
    float speed = 1;

    Vector3 startPos = Vector3.zero;
    Vector3 startRotation = Vector3.zero;

    CameraDrag cameraDrag;

    bool isTravelingBack = false;

	// Use this for initialization
	void Start () {
        cameraDrag = GetComponent<CameraDrag>();
        startPos = cameraDrag.MyStartPosition();
        startRotation = cameraDrag.MyStartEulerRotation();
	}
	
	// Update is called once per frame
	void Update ()
    {

        if (!previewMode)
        {
            if (isTravelingBack)
            {
                MoveBackToTable();
            }

            return;
        }

        MoveToSpot();
    }

    private void MoveToSpot()
    {
        cameraDrag.AllowDrag(false);

        Vector3 pos = Vector3.Lerp(transform.position, previewSpot.position, Time.deltaTime * speed);
        transform.position = pos;

        Vector3 rot = Vector3.Lerp(transform.eulerAngles, Vector3.zero, Time.deltaTime * speed);
        transform.eulerAngles = rot;
    }

    private void MoveBackToTable()
    {
        Vector3 pos = Vector3.Lerp(transform.position, startPos, Time.deltaTime * speed * 2);
        transform.position = pos;

        Vector3 rot = Vector3.Lerp(transform.eulerAngles, startRotation, Time.deltaTime * speed * 2);
        transform.eulerAngles = rot;

        if (Vector3.Distance(transform.position, startPos) < 0.1f)
        {
            transform.position = startPos;
            transform.eulerAngles = startRotation;
            cameraDrag.AllowDrag(true);
            isTravelingBack = false;
            
        }
    }

    public void EnterPreviewMode()
    {
        previewMode = true;
    }

    public void ExitPreviewMode()
    {
        isTravelingBack = true;
        previewMode = false;
    }

    public bool Previewing()
    {
        return previewMode;
    }

    
}
