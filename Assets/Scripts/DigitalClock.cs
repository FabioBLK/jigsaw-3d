﻿using UnityEngine;
using System.Collections;

public class DigitalClock : MonoBehaviour {
    TextMesh myTextMesh;
    float timer;

    bool timerStart = false;

	// Use this for initialization
	void Start () {
        myTextMesh = GetComponent<TextMesh>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!timerStart)
            return;

        timer += Time.deltaTime;

        int seconds = (int)(timer%60);
        int minutes = (int)(timer/60);
        
        string displayText = string.Format("{0:00}:{1:00}", minutes, seconds);

        myTextMesh.text = displayText;
	}

    public void StartTimer()
    {
        timerStart = true;
    }

    public void StopTimer()
    {
        timerStart = false;
    }

    public int ReturnTime()
    {
        return (int)timer;
    }

    public float ReturnFloatTime()
    {
        return timer;
    }
}
