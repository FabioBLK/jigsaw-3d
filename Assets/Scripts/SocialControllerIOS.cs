﻿#if UNITY_IOS
using UnityEngine;
using System.Collections;
using UnityEngine.SocialPlatforms;

public class SocialControllerIOS : MonoBehaviour {

	bool showWarning = false;

	[SerializeField]
	string leaderboard;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    // Use this for initialization
    void Start () {
		// Authenticate and register a ProcessAuthentication callback
		// This call needs to be made before we can proceed to other calls in the Social API
		#if UNITY_IOS
			Social.localUser.Authenticate (ProcessAuthentication);
		#endif
	}
	
	// This function gets called when Authenticate completes
	// Note that if the operation is successful, Social.localUser will contain data from the server. 
	void ProcessAuthentication (bool success) {
		if (success) {
			Debug.Log ("Authenticated, checking achievements");

			// Request loaded achievements, and register a callback for processing them
			Social.LoadAchievements (ProcessLoadedAchievements);
			UnityEngine.SocialPlatforms.GameCenter.GameCenterPlatform.ShowDefaultAchievementCompletionBanner(true);
		} 
		else
		{
			Debug.Log ("Failed to authenticate");
			showWarning = true;
		}

	}

	// This function gets called when the LoadAchievement call completes
	void ProcessLoadedAchievements (IAchievement[] achievements) {
		if (achievements.Length == 0)
			Debug.Log ("Error: no achievements found");
		else
			Debug.Log ("Got " + achievements.Length + " achievements");

		// You can also call into the functions like this
		//Social.ReportProgress ("Achievement01", 100.0, result => {
		//	if (result)
		//		Debug.Log ("Successfully reported achievement progress");
		//	else
		//		Debug.Log ("Failed to report achievement");
		//});
	}

	public void OnShowLeaderBoard(){
	
		Social.ShowLeaderboardUI ();
	
	}

    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

    public void OnAddScoreToLeaderBorad(long score){
	
		Social.ReportScore (score, leaderboard, HighScoreCheck);
	
	}

	public void SetCurrentLeaderBoard(string boardName)
	{
		leaderboard = boardName;
	}

    public void UnlockAchievement(string achievementID)
    {
        Social.ReportProgress(achievementID, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

	static private void HighScoreCheck(bool result){
	
		if (result)
			Debug.Log ("Score submission Successfully");
		else
			Debug.Log ("Score submission Failed");
	}

	public bool ShowWarning()
	{
		return showWarning;
	}

}
#endif