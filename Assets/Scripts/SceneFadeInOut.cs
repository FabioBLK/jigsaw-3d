﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneFadeInOut : MonoBehaviour
{
    public float fadeSpeed = 1.5f;          // Speed that the screen fades to and from black.

    private bool sceneEnding = false;
    private bool sceneStarting = true;      // Whether or not the scene is still fading in.

    [SerializeField]
    GameObject canvasObject;
    [SerializeField]
    GameObject warningCanvas;
	[SerializeField]
	GameObject warningCanvasIOS;

    [SerializeField]
    Image myGui;

    void Start()
    {
        sceneStarting = true;
    }


    void Update()
    {
        
        // If the scene is starting...
        if (sceneStarting)
            // ... call the StartScene function.
            StartScene();

        if (sceneEnding)
            EndScene();
    }


    void FadeToClear()
    {
        // Lerp the colour of the texture between itself and transparent.
        myGui.color = Color.Lerp(myGui.color, Color.clear, fadeSpeed * Time.deltaTime);
    }


    void FadeToBlack()
    {
        // Lerp the colour of the texture between itself and black.
        myGui.color = Color.Lerp(myGui.color, Color.black, fadeSpeed * Time.deltaTime);
    }


    void StartScene()
    {
        // Fade the texture to clear.
        FadeToClear();

        // If the texture is almost clear...
        if (myGui.color.a <= 0.05f)
        {
            // ... set the colour to clear and disable the GUITexture.
            myGui.color = Color.clear;
            myGui.enabled = false;

            // The scene is no longer starting.
            sceneStarting = false;
            
        }
    }

    public void ContinueOrShowWarning()
    {
        #if UNITY_IOS
			if (FindObjectOfType<SocialControllerIOS>().ShowWarning())
			{
				canvasObject.SetActive(false);
				warningCanvasIOS.SetActive(true);
			}
			else
	            StartSceneEnd();
        #endif

        #if UNITY_ANDROID
            if (FindObjectOfType<SocialController>().ShowWarning())
            {
                canvasObject.SetActive(false);
                warningCanvas.SetActive(true);
            }
            else
                StartSceneEnd();
        #endif
    }

    public void StartSceneEnd()
    {
        sceneEnding = true;
    }


    public void EndScene()
    {
        sceneStarting = false;
        // Make sure the texture is enabled.
        myGui.enabled = true;
        canvasObject.SetActive(false);
        // Start fading towards black.
        FadeToBlack();

        // If the screen is almost black...
        if (myGui.color.a >= 0.95f)
        {
            // ... reload the level.
            SceneController.LoadNextScene();
        }
    }
}