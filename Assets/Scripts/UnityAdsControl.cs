﻿using UnityEngine;
using System.Collections;
using GoogleMobileAds.Api;
using UnityEngine.UI;

public class UnityAdsControl : MonoBehaviour {

    InterstitialAd interstitial;
    BannerView bannerView;

    [SerializeField]
    bool debugMode = true;

    [SerializeField]
    bool startBanner = false;

    [SerializeField]
    bool startInterstitial = false;

    // Use this for initialization
    void Start () {

        if (startBanner)
            RequestBanner();

        if (startInterstitial)
            RequestInterstitial();

    }
	

    string AdUnitID(int type)
    {
#if UNITY_ANDROID
        if (type == 1)
        {
            return "ca-app-pub-1060172145249514/4341598188"; //return bunner Ad ID
        }
        else
        {
            return "ca-app-pub-1060172145249514/7295064586"; //return interstitial Ad ID
        }
#endif

#if UNITY_IOS
        if (type == 1)
        {
			return "ca-app-pub-1060172145249514/4903014588"; //return bunner Ad ID
        }
        else
        {
			return "ca-app-pub-1060172145249514/7856480981"; //return interstitial Ad ID
        }
#endif

    }

    void RequestBanner()
    {

        // Create a 320x50 banner
        bannerView = new BannerView(AdUnitID(1), AdSize.Banner, AdPosition.Bottom);
        

        if (!debugMode)
        {
            AdRequest request = new AdRequest.Builder().Build();

            bannerView.LoadAd(request);
        }
            
        else
        {
#if UNITY_ANDROID
            AdRequest request = new AdRequest.Builder()
            .Build();
#endif

#if UNITY_IOS

            AdRequest request = new AdRequest.Builder()
            .Build();
#endif

            bannerView.LoadAd(request);
        }

        // Load the banner with the request.
        
    }

    private void RequestInterstitial()
    {
        // Initialize an InterstitialAd.
        interstitial = new InterstitialAd(AdUnitID(2));
        
        if (!debugMode)
        {
            // Create an empty ad request.
            AdRequest request = new AdRequest.Builder().Build();
            // Load the interstitial with the request.
            interstitial.LoadAd(request);
        }
        else
        {
#if UNITY_ANDROID
            AdRequest request = new AdRequest.Builder()
            .Build();
#endif

#if UNITY_IOS

            AdRequest request = new AdRequest.Builder()
            .Build();
#endif
            interstitial.LoadAd(request);
        }

    }

    public void CallInterstitialAD()
    {
        if (interstitial.IsLoaded())
        {
            interstitial.Show();
        }
    }

    void DestroyLoadedBanner()
    {
        bannerView.Destroy();
    }

    void DestroyLoadedInterstitial()
    {
        interstitial.Destroy();
    }

    public void HideLoadedBanner()
    {
        bannerView.Hide();
    }

    public void ShowLoadedBanner(bool value)
    {
        if (value)
            bannerView.Show();
        else
            bannerView.Hide();
    }

#if UNITY_IOS
    public static string GetIOSAdMobID()
    {
        return Md5Sum(UnityEngine.iOS.Device.advertisingIdentifier);
    }
#endif

#if UNITY_ANDROID
    public static string GetAndroidAdMobID()
    {
        UnityEngine.AndroidJavaClass up = new UnityEngine.AndroidJavaClass("com.unity3d.player.UnityPlayer");
        UnityEngine.AndroidJavaObject currentActivity = up.GetStatic<UnityEngine.AndroidJavaObject>("currentActivity");
        UnityEngine.AndroidJavaObject contentResolver = currentActivity.Call<UnityEngine.AndroidJavaObject>("getContentResolver");
        UnityEngine.AndroidJavaObject secure = new UnityEngine.AndroidJavaObject("android.provider.Settings$Secure");
        string deviceID = secure.CallStatic<string>("getString", contentResolver, "android_id");
        return Md5Sum(deviceID).ToUpper();
    }
#endif

    public static string Md5Sum(string strToEncrypt)
    {
        System.Text.UTF8Encoding ue = new System.Text.UTF8Encoding();
        byte[] bytes = ue.GetBytes(strToEncrypt);

        System.Security.Cryptography.MD5CryptoServiceProvider md5 = new System.Security.Cryptography.MD5CryptoServiceProvider();
        byte[] hashBytes = md5.ComputeHash(bytes);

        string hashString = "";
        for (int i = 0; i < hashBytes.Length; i++)
        {
            hashString += System.Convert.ToString(hashBytes[i], 16).PadLeft(2, '0');
        }

        return hashString.PadLeft(32, '0');
    }

    void OnDestroy()
    {
        DestroyLoadedBanner();
        DestroyLoadedInterstitial();
    }
}
