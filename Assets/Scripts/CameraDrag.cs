﻿using UnityEngine;
using System.Collections;

public class CameraDrag : MonoBehaviour {

    Vector3 dragStart = Vector3.zero;

    bool drag = true;
    bool zoom = false;
    bool autoPilotMode = false;

    PicturePreview picturePreview;
    PathControl pathControl;

    Quaternion startRotation;
    Vector3 startPosition;
    Vector3 startEulerRotation;

    [SerializeField]
    float pinchSpeed = 5;

    [SerializeField]
    float dragSpeed = 1;

    [SerializeField]
    float maxXPos = 10;
    [SerializeField]
    float minXPos = -10;
    [SerializeField]
    float maxZPos = 0.5f;
    [SerializeField]
    float minZPos = -2.5f;
    [SerializeField]
    float maxRotation = 80;
    [SerializeField]
    float minRotation = 10;
    [SerializeField]
    float maxZoom = 30;
    [SerializeField]
    float minZoom = 07;

    [SerializeField]
    GameObject autoPilotHolder;

    // Use this for initialization
    void Start () {
        picturePreview = GetComponent<PicturePreview>();
        startPosition = transform.position;
        startRotation = transform.rotation;
        startEulerRotation = transform.eulerAngles;
        pathControl = FindObjectOfType<PathControl>();
        EnterAutoPilot();
    }
	
	// Update is called once per frame
	void LateUpdate () {

        if (autoPilotMode)
        {
            if (Input.GetMouseButtonDown(0))
            {
                ExitAutoPilot();
            }
            return;
        }
            

        PinchCode();
        /*
        if (!drag || zoom)
            return;

        
        if (Input.GetMouseButtonDown(0))
        {
            dragStart = Input.mousePosition;
            return;
        }

        if (Input.GetMouseButton(0) && Input.touchCount <= 1)
        {
            float newDragSpeed = dragSpeed;
            Vector3 pos = Camera.main.ScreenToViewportPoint(Input.mousePosition - dragStart);
            Vector3 move = new Vector3(pos.x * newDragSpeed, 0, pos.y * newDragSpeed);

            transform.Translate(-move, Space.World);
        }

        float yPos = Mathf.Lerp(transform.position.y, Mathf.Clamp(transform.position.y, minZoom, maxZoom), Time.deltaTime);

        transform.position = new Vector3(Mathf.Clamp(transform.position.x, minXPos, maxXPos), yPos, Mathf.Clamp(transform.position.z,minZPos,maxZPos));
        transform.eulerAngles = new Vector3(Mathf.Clamp(transform.eulerAngles.x,minRotation,maxRotation), 0, 0);
        */
	}

    public void AllowDrag(bool value)
    {
        drag = value;
    }

    void PinchCode()
    {
        if (picturePreview.Previewing())
            return;

        float pinchAmount = 0;
        float yPos = transform.position.y;
        Quaternion desiredRotation = transform.rotation;

        DetectTouchMovement.Calculate();

        if (Mathf.Abs(DetectTouchMovement.pinchDistanceDelta) > 0)
        { // zoom
            pinchAmount = DetectTouchMovement.pinchDistanceDelta;
            zoom = true;
        }
        else
        {
            zoom = false;
        }
        //if (Mathf.Abs(DetectTouchMovement.turnAngleDelta) > 0)
        //{ // rotate
        //    Vector3 rotationDeg = Vector3.zero;
        //    rotationDeg.z = -DetectTouchMovement.turnAngleDelta;
        //    desiredRotation *= Quaternion.Euler(rotationDeg);
        //}

        //Camera.main.fieldOfView += pinchAmount;

        // not so sure those will work:
        //transform.rotation = desiredRotation;
        
        transform.position += Vector3.up * -pinchAmount * Time.deltaTime * pinchSpeed;
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(transform.position.y, minZoom - 0.1f, maxZoom + 0.1f), transform.position.z);
    }

    void EnterAutoPilot()
    {
        autoPilotMode = true;
        transform.parent = autoPilotHolder.transform;
        transform.localPosition = Vector3.zero;
        transform.localRotation = Quaternion.identity;
        pathControl.SetAutoPilot();
    }

    void ExitAutoPilot()
    {
        pathControl.ExitAutoPilot();
        Invoke("RegainControlFromAutoPilot", 1);

        
    }

    void RegainControlFromAutoPilot()
    {
        autoPilotMode = false;
        transform.parent = null;
        transform.rotation = startRotation;
        transform.position = startPosition;
        FindObjectOfType<DigitalClock>().StartTimer();
    }

    public Vector3 MyStartPosition()
    {
        return startPosition;
    }

    public Vector3 MyStartEulerRotation()
    {
        return startEulerRotation;
    }

}
