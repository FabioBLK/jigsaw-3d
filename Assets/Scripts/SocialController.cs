﻿#if UNITY_ANDROID
using UnityEngine;
using System.Collections;
using GooglePlayGames;
using UnityEngine.SocialPlatforms;
public class SocialController : MonoBehaviour
{
    #region PUBLIC_VAR
    public string leaderboard;
    #endregion
    #region DEFAULT_UNITY_CALLBACKS

    bool showWarning = true;

    void Awake()
    {
        DontDestroyOnLoad(this);
    }

    void Start()
    {

#if UNITY_ANDROID
        // recommended for debugging:
        PlayGamesPlatform.DebugLogEnabled = true;

        // Activate the Google Play Games platform
        PlayGamesPlatform.Activate();

        LogIn();

#endif

    }
    #endregion
    #region BUTTON_CALLBACKS
    /// <summary>
    /// Login In Into Your Google+ Account
    /// </summary>
    public void LogIn()
    {
        Social.localUser.Authenticate((bool success) =>
        {
            if (success)
            {
                Debug.Log("Login Sucess");
                showWarning = false;
            }
            else {
                Debug.Log("Login failed");
            }
        });
    }

    public bool ShowWarning()
    {
        return showWarning;
    }

    /// <summary>
    /// Shows All Available Leaderborad
    /// </summary>
    public void OnShowLeaderBoard()
    {
        //        Social.ShowLeaderboardUI (); // Show all leaderboard
        ((PlayGamesPlatform)Social.Active).ShowLeaderboardUI(leaderboard); // Show current (Active) leaderboard
    }
    /// <summary>
    /// Adds Score To leader board
    /// </summary>
    public void OnAddScoreToLeaderBorad(long score)
    {
        print(score);
        if (Social.localUser.authenticated)
        {
            
            Social.ReportScore(score, leaderboard, (bool success) =>
            {
                if (success)
                {
                    Debug.Log("Update Score Success");

                }
                else {
                    Debug.Log("Update Score Fail");
                }
            });
        }
    }

    public void UnlockAchievement(string currentAchievement)
    {
        Social.ReportProgress(currentAchievement, 100.0f, (bool success) => {
            // handle success or failure
        });
    }

    public void SetCurrentLeaderBoard(string boardName)
    {
        leaderboard = boardName;
    }

    public void ShowMainLeaderBoard()
    {
        Social.ShowLeaderboardUI();
    }

    public void ShowAchievements()
    {
        Social.ShowAchievementsUI();
    }

    /// <summary>
    /// On Logout of your Google+ Account
    /// </summary>
    public void OnLogOut()
    {
        //((PlayGamesPlatform)Social.Active).SignOut();
    }
    #endregion
}
#endif