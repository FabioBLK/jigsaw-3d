﻿using UnityEngine;
using System.Collections;

public class TitleMovement : MonoBehaviour {

    Vector3 rot = Vector3.zero;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 acelInput = new Vector3(Input.acceleration.z+0.75f, Input.acceleration.x, 0);

        rot += acelInput;

        rot = new Vector3(Mathf.Clamp(rot.x, -10, 10), Mathf.Clamp(rot.y, -5, 5), rot.z);

        transform.localEulerAngles = rot;
        	
	}
}
