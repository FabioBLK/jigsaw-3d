﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {

    private static MusicManager _instance;
    public static MusicManager instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<MusicManager>();
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    AudioSource myAudio;

    [SerializeField]
    float maxVolume = 1;

    [SerializeField]
    float minVolume = 0;

    [SerializeField]
    AudioClip introMusic;

    [SerializeField]
    AudioClip selectImageMusic;

    [SerializeField]
    AudioClip[] gamePlayMusic;

    bool changingMusic = false;
    bool onMute = false;
    int currentSceneIndex = 0;
    int mainGameMusicIndex = 0;
    float currentVolume = 1;
    float thisMaxVolume = 0;

    void Awake()
    {
        MusicManager findOther = FindObjectOfType<MusicManager>();
        if (findOther != null)
        {
            if (findOther != this)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }

	// Use this for initialization
	void Start () {
        myAudio = GetComponent<AudioSource>();
        thisMaxVolume = maxVolume;
        ChangeMusic(1);
	}
	
	// Update is called once per frame
	void Update () {

        if (changingMusic)
        {
            myAudio.volume = currentVolume;
            currentVolume -= 0.01f;
            if (currentVolume < minVolume)
            {
                SelectMusic();
            }
        }
        else
        {
            if (currentVolume < thisMaxVolume)
            {
                //print("volume up");
                currentVolume += 0.01f;
                myAudio.volume = currentVolume;
            }
        }


	}

    private void SelectMusic()
    {
        changingMusic = false;

        if (currentSceneIndex == 1)
        {
            PlayIntro();
        }
        else if (currentSceneIndex == 2)
        {
            PlaySelectImage();
        }
        else if (currentSceneIndex == 3)
        {
            PlayMainGame();
        }
    }

    public void ChangeMusic(int index)
    {
        changingMusic = true;
        currentSceneIndex = index;
    }

    void PlayIntro()
    {
        myAudio.Stop();
        myAudio.clip = introMusic;
        myAudio.Play();
    }

    void PlaySelectImage()
    {
        myAudio.Stop();
        myAudio.clip = selectImageMusic;
        myAudio.Play();
    }

    void PlayMainGame()
    {
        myAudio.Stop();

        if (mainGameMusicIndex < gamePlayMusic.Length - 1)
            mainGameMusicIndex++;
        else
            mainGameMusicIndex = 0;

        myAudio.clip = gamePlayMusic[mainGameMusicIndex];
        
        myAudio.Play();
    }

    public void MuteMusic(bool value)
    {
        if (value)
        {
            thisMaxVolume = 0;
            currentVolume = 0;
            myAudio.volume = thisMaxVolume;
        }
            
        else
        {
            thisMaxVolume = maxVolume;
            currentVolume = maxVolume;
            myAudio.volume = thisMaxVolume;
        }
        onMute = value;
    }

    public bool IsMusicMute()
    {
        return onMute;
    }

}
