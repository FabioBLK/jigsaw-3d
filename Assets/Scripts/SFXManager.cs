﻿using UnityEngine;
using System.Collections;

public class SFXManager : MonoBehaviour {

    private static SFXManager _instance;
    public static SFXManager instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = GameObject.FindObjectOfType<SFXManager>();
                DontDestroyOnLoad(_instance.gameObject);
            }
            return _instance;
        }
    }

    AudioSource myAudio;
	
	[SerializeField]
    AudioClip clickSound;
	[SerializeField]
    AudioClip wooshSound;
	[SerializeField]
    AudioClip boxSelectSound;
	[SerializeField]
	AudioClip matchPlaceSound;
	[SerializeField]
	AudioClip congratulationsSound;

    bool onMute = false;

    void Awake()
    {
        SFXManager findOther = FindObjectOfType<SFXManager>();
        if (findOther != null)
        {
            if (findOther != this)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);
    }


    void Start()
    {
        myAudio = GetComponent<AudioSource>();
    }

    public void PlayClickSound()
    {
        myAudio.PlayOneShot(clickSound);
    }

	public void PlayWooshSound()
	{
		myAudio.PlayOneShot(wooshSound);
	}

	public void PlayBoxSelectSound()
	{
		myAudio.PlayOneShot(boxSelectSound);
	}

	public void PlayMatchPlaceSound()
	{
		myAudio.PlayOneShot(matchPlaceSound);
	}

	public void PlayCongratulationsSound()
	{
		myAudio.PlayOneShot(congratulationsSound);
	}

    public void MuteSFX(bool value)
    {
        if (value)
        {
            myAudio.volume = 0;
        }

        else
        {
            myAudio.volume = 1;
        }
        onMute = value;
    }

    public bool IsSFXMute()
    {
        return onMute;
    }
}
