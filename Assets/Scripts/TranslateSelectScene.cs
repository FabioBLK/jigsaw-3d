﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TranslateSelectScene : MonoBehaviour {

    [SerializeField]
    TextMesh selectImageText;

    [SerializeField]
    Text setPuzzleSizeText;

    [SerializeField]
    Text pieces16Text;

    [SerializeField]
    Text pieces36Text;

    [SerializeField]
    Text pieces64Text;

    [SerializeField]
    Text playButtonText;


    // Use this for initialization
    void Start () {
        StartSelectImageText();
    }

    void StartSelectImageText()
    {
        selectImageText.text = LanguageStrings.instance.TextString("select_image");
        setPuzzleSizeText.text = LanguageStrings.instance.TextString("set_puzzle_size");
        pieces16Text.text = LanguageStrings.instance.TextString("pieces_16");
        pieces36Text.text = LanguageStrings.instance.TextString("pieces_36");
        pieces64Text.text = LanguageStrings.instance.TextString("pieces_64");
        playButtonText.text = LanguageStrings.instance.TextString("play_button");
    }

}
