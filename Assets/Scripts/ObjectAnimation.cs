﻿using UnityEngine;
using System.Collections;

public class ObjectAnimation : MonoBehaviour {

    Animator myAnim;
    bool currentAnim = false;

    [SerializeField]
    bool singleAnimation = false;

	[SerializeField]
	bool startAnimating = false;

	// Use this for initialization
	void Start () {
        myAnim = GetComponent<Animator>();

		if (startAnimating)
			myAnim.SetBool ("Animate", true);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnMouseDown()
    {
        if (!singleAnimation)
        {
            currentAnim = !currentAnim;
            myAnim.SetBool("Animate", currentAnim);
        }
        else
        {
            myAnim.SetTrigger("Animate");
        }

    }
}
