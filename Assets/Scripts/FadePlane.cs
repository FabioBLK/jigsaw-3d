﻿using UnityEngine;
using System.Collections;

public class FadePlane : MonoBehaviour {

    Renderer myRender;

    Color transparentColor = new Color(0, 0, 0, 0);
    Color difuseColor = new Color(0, 0, 0, 1);

    bool fadeToBlack = false;
    bool fadeToDifuse = false;

    float alpha = 0;

	// Use this for initialization
	void Start () {
        myRender = GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {
	
        if (fadeToDifuse)
        {
            
            myRender.material.color = new Color(1, 1, 1, alpha);
            alpha+=0.025f;
            if (alpha >= 1)
                fadeToDifuse = false;
        }

        if (fadeToBlack)
        {
            
            myRender.material.color = new Color(1, 1, 1, alpha);
            alpha-=0.1f;
            if (alpha <= 0)
                fadeToBlack = false;
        }

    }

    public void StartFade()
    {
        fadeToBlack = false;
        fadeToDifuse = true;
    }

    public void StartDifuse()
    {
        fadeToBlack = true;
        fadeToDifuse = false;
    }
}
