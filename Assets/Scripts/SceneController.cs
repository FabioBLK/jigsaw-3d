﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SceneController {

    public static void CallBeginScene()
    {
        SceneManager.LoadScene("Begin");
    }

    public static void CallGameScene()
    {
        SceneManager.LoadScene("Main");
    }

    public static void LoadNextScene()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }

    public static int CurrentLoadedLevel()
    {
        return SceneManager.GetActiveScene().buildIndex;
    }

}
