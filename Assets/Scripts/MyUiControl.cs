﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MyUiControl : MonoBehaviour {

    bool previewPressed = true;

    [SerializeField]
    UnityAdsControl unityAdsControl;

    [SerializeField]
    GameObject goButton;
    [SerializeField]
    GameObject backButton;
    [SerializeField]
    GameObject pauseButton;
    [SerializeField]
    GameObject resumeButton;
    [SerializeField]
    GameObject pausePanel;
    [SerializeField]
    GameObject gameOverPanel;
    [SerializeField]
    Text timeText;


    public void PreviewButtonPressed()
    {
        SFXManager.instance.PlayClickSound();
        previewPressed = !previewPressed;
        unityAdsControl.ShowLoadedBanner(!previewPressed);
        backButton.SetActive(!previewPressed);
        goButton.SetActive(previewPressed);
    }

    public void PausePressed()
    {
        SFXManager.instance.PlayClickSound();
        unityAdsControl.ShowLoadedBanner(true);
        pausePanel.SetActive(true);
        resumeButton.SetActive(true);
        goButton.SetActive(false);
        backButton.SetActive(false);
        pauseButton.SetActive(false);
    }

    public void ResumePressed()
    {
        SFXManager.instance.PlayClickSound();
        unityAdsControl.ShowLoadedBanner(false);
        pausePanel.SetActive(false);
        pauseButton.SetActive(true);

        goButton.SetActive(previewPressed);
        backButton.SetActive(!previewPressed);

        resumeButton.SetActive(false);
    }

    public void GameOverScreen()
    {
        goButton.SetActive(false);
        backButton.SetActive(false);
        pauseButton.SetActive(false);
        resumeButton.SetActive(false);
        pausePanel.SetActive(false);
        Invoke("ShowGameOverPanel", 2);
    }

    void ShowGameOverPanel()
    {
        gameOverPanel.SetActive(true);

    }

    public void CallSelectionScene()
    {
        SFXManager.instance.PlayClickSound();
        unityAdsControl.CallInterstitialAD();
        MusicManager.instance.ChangeMusic(2);
        Destroy(FindObjectOfType<SelectionManager>().gameObject);
        SceneManager.LoadScene("Begin");
    }

    public void CallTitleScene()
    {
        SFXManager.instance.PlayClickSound();

#if UNITY_IOS
		Destroy(FindObjectOfType<SocialControllerIOS>().gameObject);
#endif

#if UNITY_ANDROID
        Destroy(FindObjectOfType<SocialController>().gameObject);
#endif
        MusicManager.instance.ChangeMusic(1);
        Destroy(FindObjectOfType<SelectionManager>().gameObject);
        SceneManager.LoadScene("Title", LoadSceneMode.Single);
    }

    public void SetFinalTime(int time)
    {
        int seconds = (int)(time % 60);
        int minutes = (int)(time / 60);
        string displayText = string.Format("{0:00}:{1:00}", minutes, seconds);
        timeText.text = displayText;
    }
}
