﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TranslateMainScene : MonoBehaviour {

    [SerializeField]
    Text musicText;

    [SerializeField]
    Text sfxText;

    [SerializeField]
    Text imageSelectText;

    [SerializeField]
    Text titleScreenText;

    [SerializeField]
    Text playAgainText;

    [SerializeField]
    Text leaderboardsText;

    [SerializeField]
    Text congratulationsText;

    // Use this for initialization
    void Start () {
        StartMainGameText();
    }

    void StartMainGameText()
    {
        musicText.text = LanguageStrings.instance.TextString("music");
        sfxText.text = LanguageStrings.instance.TextString("sfx");
        imageSelectText.text = LanguageStrings.instance.TextString("new_image");
        titleScreenText.text = LanguageStrings.instance.TextString("title_screen");
        playAgainText.text = LanguageStrings.instance.TextString("play_again");
        leaderboardsText.text = LanguageStrings.instance.TextString("leaderboards");
        congratulationsText.text = LanguageStrings.instance.TextString("congratulations_full");
    }

}
