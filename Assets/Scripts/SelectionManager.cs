﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class SelectionManager : MonoBehaviour {

    [SerializeField]
    GameObject[] puzzleBoxes;

    [SerializeField]
    Texture selectedTexture;

    [SerializeField]
    int sizeSelected = 0; //This number highlights the default pieces select button (2 = 36 pieces)

    [SerializeField]
    GameObject canvasMenu;

    [SerializeField]
    FadePlane fadePlane;

    BoxCollider[] colliders;

    ItemSelect itemSelect;

    bool findButtons = false;
    HighlightButton[] buttons;
    HighlightButton button16;
    HighlightButton button36;
    HighlightButton button64;

    void Awake()
    {
        DontDestroyOnLoad(this);
        Time.timeScale = 1;
    }

    void Start()
    {
        colliders = new BoxCollider[puzzleBoxes.Length];

        for (int i = 0; i < puzzleBoxes.Length; i++)
        {
            colliders[i] = puzzleBoxes[i].GetComponent<BoxCollider>();
        }
    }

    void Update()
    {
        if (findButtons)
        {
            if (sizeSelected == 1)
                button16.HighLightThis();
            else if (sizeSelected == 2)
                button36.HighLightThis();
            else if (sizeSelected == 3)
                button64.HighLightThis();
        }
    }
	
    public void BoxSelected(ItemSelect item, Texture itemTexture)
    {
        DisableColliders();
        selectedTexture = itemTexture;
        itemSelect = item;
        canvasMenu.SetActive(true);
        fadePlane.StartFade();
        buttons = FindObjectsOfType<HighlightButton>();

        for (int i =0; i< buttons.Length; i++)
        {
            if (buttons[i].MyCurrentNumber() == 1)
                button16 = buttons[i];
            else if (buttons[i].MyCurrentNumber() == 2)
                button36 = buttons[i];
            else if (buttons[i].MyCurrentNumber() == 3)
                button64 = buttons[i];
        }

        findButtons = true;
    }

    void DisableColliders()
    {
        foreach (BoxCollider col in colliders)
        {
            col.enabled = false;
        }
        
    }

    public void EnableColliders()
    {
        SFXManager.instance.PlayClickSound();
        foreach (BoxCollider col in colliders)
        {
            col.enabled = true;
        }
        itemSelect.GoToShelf();
        canvasMenu.SetActive(false);
        fadePlane.StartDifuse();
        findButtons = false;
    }

    public void PiecesSelected(int number)
    {
        SFXManager.instance.PlayClickSound();
        sizeSelected = number;
    }

    public void PrepareForNextScene()
    {
        SFXManager.instance.PlayClickSound();
        MusicManager.instance.ChangeMusic(3);
        findButtons = false;
        //SceneManager.LoadScene("Main");
    }

    public Texture SelectedTexture()
    {
        return selectedTexture;
    }

    public int SelectedPuzzleSize()
    {
        return sizeSelected;
    }
}
