﻿using UnityEngine;
using System.Collections;
using System.IO;

public class LanguageStrings : MonoBehaviour {

    private static LanguageStrings _instance;
    public static LanguageStrings instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = FindObjectOfType<LanguageStrings>();
                DontDestroyOnLoad(_instance);
            }
            return _instance;
        }
    }

    LanguageControl languageControl;

    [SerializeField]
    string currentLanguage = "English";

    void Awake()
    {
        LanguageStrings languageStrings = FindObjectOfType<LanguageStrings>();
        if (languageStrings != null)
        {
            if (languageStrings != this)
                Destroy(this.gameObject);
        }
        DontDestroyOnLoad(this);

        currentLanguage = Application.systemLanguage.ToString();
    }

	// Use this for initialization
	void Start () {
        //languageControl = new LanguageControl(Path.Combine(Application.dataPath, "lang.xml"), currentLanguage, false);
        languageControl = new LanguageControl("lang", currentLanguage, false);
        
        if (!languageControl.IsDataLoaded())
        {
            currentLanguage = "English";
            languageControl = new LanguageControl("lang", currentLanguage, false);
        }
            
	}

    public string TextString(string stringName)
    {
        return languageControl.getString(stringName);
    }
}
