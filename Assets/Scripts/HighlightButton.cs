﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HighlightButton : MonoBehaviour {

    Button myButton;
    [SerializeField]
    int myNumber = 2;
    bool isThis = false;

    public void HighLightThis()
    {
        //isThis = true;
        myButton = GetComponent<Button>();
        myButton.Select();
    }

    public int MyCurrentNumber()
    {
        return myNumber;
    }
}