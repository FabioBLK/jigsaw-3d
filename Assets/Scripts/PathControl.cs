﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PathControl : MonoBehaviour {

    [SerializeField]
    GameObject[] path;

    [SerializeField]
    GameObject playerPosition;

    [SerializeField]
    Transform[] lookAt;

    [SerializeField]
    GameObject fadePlane;

    [SerializeField]
    Canvas currentUI;

    [SerializeField]
    float speed = 1;

    [SerializeField]
    float transitionSpeed = 5;

    [SerializeField]
    bool allowCycle = true;

    [SerializeField]
    Camera myCamera;

    bool lastAllowCycle = false;
    float destDistance = 1;
    int pathNumber = -1;
    int currentPathPoint = 0;
    float transitionStep = 0.1f;
    bool transition = false;
    bool backToGame = false;

    
    bool autoPilot = false;

    GameObject currentPath;
    Transform[] pathPoint;

    Vector3 startPosition = Vector3.zero;
    
    Renderer fadePlaneMaterial;

    // Use this for initialization
    void Start () {

        //startPosition = playerPosition.transform.position;
        ChangeCurrentPath();
        //myCamera = GameObject.Find("Main Camera").GetComponent<Camera>();
        fadePlaneMaterial = fadePlane.GetComponent<Renderer>();
	}
	
	// Update is called once per frame
	void Update () {

        if (!autoPilot)
        {
            return;
        }

        
        if (Input.GetKeyDown(KeyCode.Return) || backToGame)
        {
            FindObjectOfType<UnityAdsControl>().HideLoadedBanner();
            transition = true;
        }

        if (transition)
        {
            if (transitionStep == 0)
                SetPlanePosition(true);

            if (transitionStep < 1)
            {
                FadeIn();
            }
            else
            {
                PathTransition();
            }
            
            return;
        }
        
        if (transitionStep > 0)
        {
            FadeOut();
        }    

        float dir = Vector3.Distance(playerPosition.transform.position, pathPoint[currentPathPoint].position);
        playerPosition.transform.position = Vector3.MoveTowards(playerPosition.transform.position, pathPoint[currentPathPoint].position, Time.deltaTime * speed);
        playerPosition.transform.LookAt(lookAt[pathNumber]);

        //print(currentPathPoint + " " + pathPoint[currentPathPoint]);
        if (dir <= destDistance)
        {
            if (currentPathPoint < pathPoint.Length - 1)
                currentPathPoint++;
            else
            {
                if (allowCycle)
                    transition = true;
            } 
        }   
	}

    private void FadeIn()
    {
        //print(transitionStep);
        Color panelColor = new Color(0, 0, 0, transitionStep);
        transitionStep += 0.1f * Time.deltaTime * transitionSpeed;
        fadePlaneMaterial.material.color = panelColor;
    }

    private void FadeOut()
    {
        //print(transitionStep);
        Color panelColor = new Color(0, 0, 0, transitionStep);
        transitionStep -= 0.1f * Time.deltaTime * transitionSpeed;
        fadePlaneMaterial.material.color = panelColor;
        if (transitionStep < 0.2f)
        {
            SetPlanePosition(false);
            transitionStep = 0;
        }
    }

    public void ChangeCurrentPath()
    {
        if (pathNumber >= path.Length - 1)
            pathNumber = 0;
        else
            pathNumber++;

        
        currentPath = path[pathNumber];
        currentPathPoint = 0;

        pathPoint = new Transform[currentPath.transform.childCount];

        for (int i = 0; i < currentPath.transform.childCount; i++)
        {
            pathPoint[i] = currentPath.transform.GetChild(i).transform;
            //print(pathPoint[i]);
        }

        startPosition = pathPoint[0].transform.position;

    }

    void PathTransition()
    {
        if (backToGame)
        {
            autoPilot = false;
            fadePlaneMaterial.material.color = new Color(0,0,0,0);
            SetPlanePosition(false);
            transitionStep = 0;
            currentUI.enabled = true;
            return;
        }

        ChangeCurrentPath();
        playerPosition.transform.position = startPosition;
        //transitionStep = 0;
        transition = false;

    }

    void SetPlanePosition(bool value)
    {
        if (value)
        {
            fadePlane.transform.parent = myCamera.transform;
            fadePlane.transform.localPosition = new Vector3(0, 0, 0.5f);
            fadePlane.transform.localEulerAngles = new Vector3(-90, 0, 0);
        }
        else
        {
            fadePlane.transform.parent = gameObject.transform;
            fadePlane.transform.localPosition = new Vector3(500, 0, 0.5f);
            fadePlane.transform.localEulerAngles = new Vector3(-90, 0, 0);
        }
    }

    public void SetAutoPilot()
    {
        currentUI.enabled = false;
        autoPilot = true;
        backToGame = false;
        SetPlanePosition(autoPilot);
        //print("Ligando Auto");
    }

    public void ExitAutoPilot()
    {
        backToGame = true;
    }

}
