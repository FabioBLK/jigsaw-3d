#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>

#if UNITY_VERSION <= 434
#import "iPhone_View.h"

#endif

char video_url_path[1024];

@interface NonRotatingUIImagePickerController : UIImagePickerController


@end

@implementation NonRotatingUIImagePickerController
- (NSUInteger)supportedInterfaceOrientations{
    return UIInterfaceOrientationMaskLandscape;
}
@end


//-----------------------------------------------------------------

@interface APLViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate>{
    
    UIImagePickerController *imagePickerController;
@public
    const char *callback_game_object_name ;
    const char *callback_function_name ;
}

@end


@implementation APLViewController

+ (UIImage*)imageWithImage:(UIImage*)image
              scaledToSize:(CGSize)newSize;
{
    UIGraphicsBeginImageContext( newSize );
    [image drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return newImage;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self showImagePickerForSourceType:UIImagePickerControllerSourceTypePhotoLibrary];
    
}


- (void)showImagePickerForSourceType:(UIImagePickerControllerSourceType)sourceType
{
    imagePickerController = [[UIImagePickerController alloc] init];
    imagePickerController.modalPresentationStyle = UIModalPresentationCurrentContext;
    imagePickerController.sourceType = sourceType;
    imagePickerController.mediaTypes = [[NSArray alloc] initWithObjects:(NSString *)kUTTypeImage,nil];
    
    imagePickerController.delegate = self;
    
    [self.view addSubview:imagePickerController.view];
}


#pragma mark - UIImagePickerControllerDelegate

// This method is called when an image has been chosen from the library or taken from the camera.
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    
    NSString *type = [info objectForKey:UIImagePickerControllerMediaType];
    //NSLog(@"type=%@",type);
    if ([type isEqualToString:(NSString *)kUTTypeVideo] ||
        [type isEqualToString:(NSString *)kUTTypeMovie])
    {// movie != video
        NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        NSLog(@"%@", urlvideo);
        NSString *urlString = [urlvideo absoluteString];
        const char* cp = [urlString UTF8String];
        strcpy(video_url_path, cp);
    }
    
    //Zoomed or scrolled image if picker.editing = YES;
    UIImage *myEditedImage = [info objectForKey:UIImagePickerControllerEditedImage];
    // Original Image
    UIImage *OriginalImage = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    UIImage *imageToSave;
    
    if (OriginalImage) {
        imageToSave = OriginalImage;
    } else {
        imageToSave = myEditedImage;
    }
    
    CGSize mySize = CGSizeMake(1024, 1024);
    UIGraphicsBeginImageContext( mySize );
    [imageToSave drawInRect:CGRectMake(0,0,mySize.width,mySize.height)];
    UIImage* newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    // You can directly use this image but in case you want to store it some where
    NSString *docDirPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    NSString *filePath =  [docDirPath stringByAppendingPathComponent:@"myImage.jpg"];
    NSLog (@"File Path = %@", filePath);
    // Get PNG data from following method
    //NSData *myData =     UIImagePNGRepresentation(myEditedImage);
    NSData *myData =     UIImageJPEGRepresentation(newImage, 0.8f);
    // It is better to get JPEG data because jpeg data will store the location and other related information of image.
    if([myData writeToFile:filePath atomically:YES]){
        // Now you can use filePath as path of your image. For retrieving the image back from the path
        //UIImage *imageFromFile = [UIImage imageWithContentsOfFile:filePath];
        
        const char* cp1 = [filePath UTF8String];
        strcpy(video_url_path, cp1);
        
        [self dismissViewControllerAnimated:YES completion:NULL];
        
        // UnitySendMessage("GameObject", "VideoPicked", video_url_path);
        UnitySendMessage("ImageLoader", callback_function_name, video_url_path);
    }
    else{
        NSLog(@"Nao foi dessa vez");
        [self dismissViewControllerAnimated:YES completion:NULL];
    }
    
}


- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}


@end





extern "C" {
    
    void OpenVideoPicker(const char *game_object_name, const char *function_name) {
        
        
        if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
            // APLViewController
            UIViewController* parent = UnityGetGLViewController();
            APLViewController *uvc = [[APLViewController alloc] init];
            uvc->callback_game_object_name = strdup(game_object_name) ;
            uvc->callback_function_name = strdup(function_name) ;
            [parent presentViewController:uvc animated:YES completion:nil];
        }
    }
}